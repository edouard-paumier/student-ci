# Rapport de notation

## Qualité

* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7
* Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3
* Couverture de tests: 0.7

Coefficient de qualité = 61.25%

## Versions

### Version 1

* Total: 5
* Succès: 1
* Taux: 0.2

### Version 2

* Total: 4
* Succès: 2
* Taux: 0.5

### Total rendus

Taux moyen = 0.35

Note intermédiaire = 1.0/3

## Rendu complet

* Total: 10
* Succès: 4
* Taux: 0.4

Note intermédiaire = 4.0/10

## Total 

Sous-total: 5.0/13

Impact qualité: 81%

Total: 4.03/13
