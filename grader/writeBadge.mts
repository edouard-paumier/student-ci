import axios from 'axios';
import { promises as fs } from 'fs';

export async function writeBadge(branch: string, badge: string, value: string) {
  const badgeContent = await requestBadge(branch, badge, value)
  if (!badgeContent)
    return

  await fs.writeFile(badge + ".svg", badgeContent)
}

async function requestBadge(branch: string, badge: string, value: string) {
  const color = chooseBadgeColor(branch, badge)
  if (!color)
    return

  const encodedName = `${encodeForBadge(badge)}%20de%20${encodeForBadge(branch)}`
  const encodedValue = `${encodeForBadge(value)}`

  const response = await axios.default.get(`https://img.shields.io/badge/${encodedName}-${encodedValue}-${color}.svg`)
  return response.data
}

function encodeForBadge(value: string) {
  return encodeURIComponent(value.replace(/-/g, '--'))
}

function chooseBadgeColor(branch: string, badge: string) {
  switch (`${branch}/${badge}`) {
    case "prod/note": return "e05d44"; // bright red
    case "main/note": return "a3311b"; // dark red
    case "master/note": return "a3311b"; // dark red    
    case "prod/completude": return "fe7d37"; // bright orange
    case "main/completude": return "ce4901"; // dark orange
    case "master/completude": return "ce4901"; // dark orange    
    case "prod/livraison": return "dfb317"; // bright yellow
    case "main/livraison": return "83690d"; // dark yellow
    case "master/livraison": return "83690d"; // dark yellow    
    case "prod/qualite": return "007ec6"; // bright blue
    case "main/qualite": return "003d60"; // dark blue
    case "master/qualite": return "003d60"; // dark blue    
    case "prod/couverture": return "97ca00"; // bright green
    case "main/couverture": return "4b6400"; // dark green
    case "master/couverture": return "4b6400"; // dark green    
  }
}
