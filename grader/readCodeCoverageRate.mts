import { promises as fs } from 'fs'

export async function readCodeCoverageRate() {
  const content = await readCodeCoverageFile()
  const value = +content
  return !isNaN(value)
    ? value
    : 0
}

async function readCodeCoverageFile() {
  try {
    const buffer = await fs.readFile('coverage/line_rate.txt')
    return buffer.toString()
  } catch {
    console.warn("No code coverage, defaulting to 0")
    return "0"
  }
}
