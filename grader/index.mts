import { computeReport } from './computeReport.mjs'
import { writeBadge } from './writeBadge.mjs'
import { writeMarkdown } from './writeMarkdown.mjs'
import { writePage } from './writePage.mjs'

import { argv } from 'node:process';

async function main(branch: string, currentVersion: number, maximumVersion: number) {
  const result = await computeReport(currentVersion, maximumVersion)
  await writeMarkdown(result)    
  await writeBadge(branch, "couverture", (result.quality.criteria['Couverture de tests'] * 100.0).toFixed(0) + "%")    
  await writeBadge(branch, "qualite", (result.quality.rate * 100.0).toFixed(0) + "%")    
  await writeBadge(branch, "completude", (result.tests.full.rate * 100.0).toFixed(0) + "%")    
  await writeBadge(branch, "note", (result.total.value).toFixed(2) + "/" + (result.total.maximum).toFixed(2))    
  await writePage(branch, result)
  console.log("Generation complete")
}

main(argv[2], parseInt(argv[3]), parseInt(argv[4]))
