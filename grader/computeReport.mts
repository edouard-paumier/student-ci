import { computeQualityReport } from './computeQualityReport.mjs'
import { readFullTestReport, readVersionReport } from './readIndividualTestReport.mjs'
import type { Grade, VersionListReport } from '../reportSchema.js'


export async function computeReport(currentVersion: number, maximumVersion: number) {
  const quality = await computeQualityReport()
  const full = await readFullTestReport()

  const versions: VersionListReport = {}
  const subTotal: Grade = {...full.grade}
  for (let version = 1; version <= maximumVersion; version++) {
    const result = await readVersionReport(version, currentVersion, maximumVersion) 
    versions['Version ' + version] = result

    if (result.status !== 'future') {
      subTotal.value += result.grade.value
      subTotal.maximum += result.grade.maximum
    }
  }
  
  return {
    quality,
    tests: {
      versions,
      full,
    },
    subTotal,
    total: {
      value: subTotal.value * (1.0 - quality.penalty),
      maximum: subTotal.maximum
    }
  }
}
