import { vi } from 'vitest'

export default {
  readFile: vi.fn()
}
