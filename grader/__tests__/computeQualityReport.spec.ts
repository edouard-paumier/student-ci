import { describe, it, expect, vi } from 'vitest'
import computeQualityReport from '../computeQualityReportm'
import readQualityReport from '../readQualityReport.mjs'
import readCodeCoverageRate from '../readCodeCoverageRate.mjs'

vi.mock('../readQualityReport')
vi.mock('../readCodeCoverageRate')

describe('computeQualityReport', () => {
  it('includes the criteria read', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });
    vi.mocked(readCodeCoverageRate).mockResolvedValueOnce(0.7);

    const result = await computeQualityReport();

    expect(Object.keys(result.criteria)).toContain('criteria1')
    expect(result.criteria.criteria1).toBe(0.8)
    expect(Object.keys(result.criteria)).toContain('criteria2')
    expect(result.criteria.criteria2).toBe(0.2)
  })

  it('includes the criteria code coverage read', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });
    vi.mocked(readCodeCoverageRate).mockResolvedValue(0.7);

    const result = await computeQualityReport();

    expect(Object.keys(result.criteria)).toContain('Couverture de tests')
    expect(result.criteria['Couverture de tests']).toBe(0.7)
  })

  it('includes the criteria code coverage read', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });
    vi.mocked(readCodeCoverageRate).mockResolvedValue(0.7);

    const result = await computeQualityReport();

    expect(Object.keys(result.criteria)).toContain('Couverture de tests')
    expect(result.criteria['Couverture de tests']).toBe(0.7)
  })

  it('includes only those 3 criteria', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });

    const result = await computeQualityReport();

    expect(Object.keys(result.criteria).length).toBe(3)
  })

  it('computes the rate as the average of all 3 criteria', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });
    vi.mocked(readCodeCoverageRate).mockResolvedValue(0.8);

    const result = await computeQualityReport();

    expect(result.rate).toBe(0.6)
  })

  it('computes the penalty as half the missing rate', async () => {
    vi.mocked(readQualityReport).mockResolvedValueOnce({
        criteria1: 0.8,
        criteria2: 0.2
    });
    vi.mocked(readCodeCoverageRate).mockResolvedValue(0.8);

    const result = await computeQualityReport();

    expect(result.rate).toBe(0.6)
    expect(result.penalty).toBe(0.2)
  })
})
