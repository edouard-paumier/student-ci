import { describe, it, expect, vi, beforeEach } from 'vitest'

vi.mock('../computeQualityReport')
vi.mock('../readIndividualTestReport')

import computeQualityReport from '../computeQualityReportm'
import computeReport from '../computeReport.mjs'
import { readFullTestReport, readVersionReport } from '../readIndividualTestReport.mjs'
import { FullTestReport, FutureVersionReport, Grade, GradedVersionReport, QualityReport, TestReport } from '../../reportSchema'

type Scenario = {
  description: string;
  quality: QualityReport;
  full: FullTestReport;
  version1: GradedVersionReport;
  version2: GradedVersionReport;
  version3: FutureVersionReport;
  version4: FutureVersionReport;
  expectedSubtotal: Grade;
  expectedTotal: Grade;
}

const scenarii: Scenario[] = [{ 
    description: 'consistent',
    quality: {
      criteria: {
        criteria1: 0.7,
        criteria2: 0.9
      },
      rate: 0.8,
      penalty: 0.1
    },
    full: {
      success: 16,
      total: 20,
      rate: 0.8,
      grade: {
        value: 8,
        maximum: 10
      }
    },
    version1: {
      status: 'locked',
      success: 2,
      total: 5,
      rate: 0.4,
      grade: {
        value: 1.0,
        maximum: 2.5
      }
    },
    version2: {
      status: 'current',
      success: 8,
      total: 10,
      rate: 0.8,
      grade: {
        value: 2.0,
        maximum: 2.5
      }
    },
    version3: {
      status: 'future',
      grade: {
        maximum: 2.5
      }
    },
    version4: {
      status: 'future',
      grade: {
        maximum: 2.5
      }
    },
    expectedSubtotal: {
      value: 11.0,
      maximum: 15.0
    },
    expectedTotal: {
      value: 9.9,
      maximum: 15.0
    }
  },{ 
    description: 'inconsistent',
    quality: {
      criteria: {},
      rate: 0,
      penalty: 0.2
    },
    full: {
      success: 0,
      total: 0,
      rate: 0,
      grade: {
        value: 4,
        maximum: 6
      }
    },
    version1: {
      status: 'current',
      success: 0,
      total: 0,
      rate: 0,
      grade: {
        value: 1,
        maximum: 3
      },
      error: 'caramba'
    },
    version2: {
      status: 'locked',
      success: 0,
      total: 0,
      rate: 0,
      grade: {
        value: 3,
        maximum: 2
      },
      error: 'encore raté'
    },
    version3: {
      status: 'future',
      grade: {
        maximum: 12
      },
    },
    version4: {
      status: 'future',
      grade: {
        maximum: 42
      }
    },
    expectedSubtotal: {
      value: 8,
      maximum: 11
    },
    expectedTotal: {
      value: 6.4,
      maximum: 11
    }
  }
]

describe('computeReport', () => {
  for (const { 
    description, 
    quality, 
    full, 
    version1, 
    version2, 
    version3,
    version4,
    expectedSubtotal,
    expectedTotal
  } of scenarii ){

    describe(`for ${description} scenario`, () => {
      beforeEach(() => {
        vi.mocked(computeQualityReport).mockResolvedValueOnce(quality)
        vi.mocked(readFullTestReport).mockResolvedValueOnce(full)
        vi.mocked(readVersionReport)
          .mockResolvedValueOnce(version1)
          .mockResolvedValueOnce(version2)
          .mockResolvedValueOnce(version3)
          .mockResolvedValueOnce(version4)
      })

      it('reads once the quality report', async () => {
        await computeReport(2, 4)
        expect(computeQualityReport).toHaveBeenCalledOnce()
      })
      
      it('reads once the full test report', async () => {
        await computeReport(2, 4)
        expect(readFullTestReport).toHaveBeenCalledOnce()
      })
      
      it('reads once each version reports', async () => {
        await computeReport(2, 4)
        expect(readVersionReport).toHaveBeenCalledTimes(4)
        expect(readVersionReport).toHaveBeenNthCalledWith(1, 1, 2, 4)
        expect(readVersionReport).toHaveBeenNthCalledWith(2, 2, 2, 4)
        expect(readVersionReport).toHaveBeenNthCalledWith(3, 3, 2, 4)
        expect(readVersionReport).toHaveBeenNthCalledWith(4, 4, 2, 4)
      })
      
      it('aggregates the result of each call in its result', async () => {
        const result = await computeReport(2, 4)
        expect(result.quality).toBe(quality)
        expect(result.tests.full).toBe(full)
        
        expect(result.tests.versions['Version 1']).toBe(version1)
        expect(result.tests.versions['Version 2']).toBe(version2)
        expect(result.tests.versions['Version 3']).toBe(version3)
        expect(result.tests.versions['Version 4']).toBe(version4)
      })
      
      it('sums the result of each grade for the subtotal', async () => {
        const result = await computeReport(2, 4)
        expect(result.subTotal).toStrictEqual(expectedSubtotal)
      })
      
      it('applies the penaly on the total', async () => {
        const result = await computeReport(2, 4)
        expect(result.total).toStrictEqual(expectedTotal)
      })
    })
  }
})