import { describe, it, expect, vi } from 'vitest'
import { promises as fs } from 'fs'
vi.mock('fs')

import readCodeCoverageRate from '../readCodeCoverageRate.mjs'

describe('readCodeCoverageRate', () => {
  it('returns 0 when failing to read the file', async () => {
    vi.mocked(fs).readFile.mockRejectedValueOnce(new Error())

    const result = await readCodeCoverageRate()
    expect(result).toBe(0)

    expect(fs.readFile).toHaveBeenCalledWith('coverage/line_rate.txt')
  })

  it('returns parsed content when file contains float to', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("0.42")

    const result = await readCodeCoverageRate()
    expect(result).toBe(0.42)

    expect(fs.readFile).toHaveBeenCalledWith('coverage/line_rate.txt')
  })

  it('returns 0 when files contain something else', async () => {
    vi.mocked(fs).readFile.mockResolvedValueOnce("toto")

    const result = await readCodeCoverageRate()
    expect(result).toBe(0)

    expect(fs.readFile).toHaveBeenCalledWith('coverage/line_rate.txt')
  })
})
