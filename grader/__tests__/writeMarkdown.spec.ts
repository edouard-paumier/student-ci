import { describe, it, expect } from 'vitest'
import { generateMarkdown } from '../writeMarkdown';
import type { Report } from "../../reportSchema";

const standardReport: Report = {
  quality: {
    rate: 0.6125,
    criteria: {
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
      'Couverture de tests': 0.7
    },
    penalty: .19,
  },
  tests: {
    versions: {
      'Version 1': {
        total: 5,
        success: 1,
        rate: 0.2,
        grade: {
          value: 0.4,
          maximum: 2
        },
        status: 'locked'
      },
      'Version 2': {
        total: 4,
        success: 2,
        rate: 0.5,
        grade: {
          value: 1,
          maximum: 2
        },
        status: 'current'
      },
      'Version 3': {
        status: 'future',
        grade: {
          maximum: 2
        }
      },
      'Version 4': {
        status: 'future',
        grade: {
          maximum: 2
        }
      },
      'Version 5': {
        status: 'future',
        grade: {
          maximum: 2
        }
      }
    },
    full: {
      total: 10,
      success: 4,
      rate: 0.4,
      grade: {
        value: 4,
        maximum: 10
      }
    }
  },
  subTotal: {
    value: 5.4,
    maximum: 14
  },
  total: {
    value: 4.374,
    maximum: 14
  }
}

const expectedMarkdown = `# Rapport de notation

## Qualité

* Nommage variables: 0.9
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.7
* Simplicité des classes: 0.6
* Pas de dupplication de code: 0.5
* Séparation de responsabilités: 0.4
* Encapsulation: 0.3
* Couverture de tests: 0.7

Coefficient de qualité = 61.25%

## Versions

### Version 1

* Total: 5
* Succès: 1
* Taux: 0.2

### Version 2

* Total: 4
* Succès: 2
* Taux: 0.5

### Total rendus

Taux moyen = 0.35

Note intermédiaire = 1.4/4

## Rendu complet

* Total: 10
* Succès: 4
* Taux: 0.4

Note intermédiaire = 4.0/10

## Total 

Sous-total: 5.4/14

Impact qualité: 81%

Total: 4.37/14
`

describe('generateMarkdown', () => {
  it('generate the right report for the given example', () => {
    const result = generateMarkdown(standardReport)
    expect(result).toBe(expectedMarkdown)
  })
})
