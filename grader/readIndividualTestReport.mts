import { promises as fs } from 'fs'
import { parseKeyValuePairLines } from './parseKeyValuePairLines.mjs'
import type { FullTestReport, VersionReport } from '../reportSchema.js'

export async function readVersionReport(version: number, currentVersion: number, maximumVersion: number): Promise<VersionReport> {
  const maximum = 10.0 / maximumVersion

  const result = await readTestResult('version' + version)

  if ('error' in result && version > currentVersion) {
    return {
      status: 'future',
      grade: { maximum }
    }
  }

  return {
    status: version < currentVersion 
      ? 'locked'
      : 'current',
    ...computeTestResult(result, maximum)
  }
}

type TestResult = {
  success: number,
  total: number
} | {
  error: string
}

async function readTestResult(testName: string): Promise<TestResult> {
  const content = await readTestFile(testName)
  if (!content)
    return { error: "Le résultat des tests n'est pas disponnible" }
  
  const parsedContent = parseKeyValuePairLines(content.split('\n'))
  if (!('Total' in parsedContent) || !('Succès' in parsedContent)) {
    return { error: "Le résultat des tests n'est pas formé correctement" }
  }
  
  return {
    success: parsedContent.Succès,
    total: parsedContent.Total,
  }
}

async function readTestFile(testName: string) {
  try {
    const buffer = await fs.readFile(`tests/${testName}.txt`)
    return buffer.toString()
  } catch {
    console.warn("No code coverage, defaulting to 0")
    return null
  }
}


export async function readFullTestReport(): Promise<FullTestReport> {
  const result = await readTestResult('full')
  return computeTestResult(result, 10)
}

function computeTestResult(result: TestResult, maximum: number) {
  if ('error' in result) {
    return {
      success: 0,
      total: 0,
      rate: 0,
      grade: { 
        value: 0,
        maximum
      },
      error: result.error
    }
  }

  const isTestRunSuccessful = !isNaN(result.total) && result.total > 0
  const rate = isTestRunSuccessful
    ? result.success / result.total
    : 0

  // Slight hack: it so happens that FullTestReport is fully a subset 
  // of VersionReport, so this is a good intersection type of both.
  // And we do need a type so that processedResult can have an error
  // property. So instead of having to create a common type temporarily 
  // here, I decided to, for now, reuse FullTestReport
  const processedResult: FullTestReport = {
    success: result.success,
    total: result.total,
    rate,
    grade: {
      value: rate * maximum, 
      maximum 
    }
  }

  if (!isTestRunSuccessful) {
    processedResult.error = "Les tests n'ont pas pu s'exécuter correctement"
  }

  return processedResult;
}
