import type { Grade, QualityCriterias, Report, VersionListReport, VersionReport } from "../reportSchema.js";

import { promises as fs } from 'fs'

export async function writeMarkdown(report: Report) {
  await fs.writeFile("report.md", generateMarkdown(report))
}

export function generateMarkdown(report: Report) {
  let sumOfRates = 0
  let sumOfGrades: Grade = {
    value: 0,
    maximum: 0
  }
  let versionGradedCount = 0

  const versions = Object.values(report.tests.versions)
  for (const version of versions) {
    if (version.status !== 'future') {
      sumOfRates += version.rate
      sumOfGrades.value += version.grade.value
      sumOfGrades.maximum += version.grade.maximum
      versionGradedCount ++
    }
  }

  return `# Rapport de notation

## Qualité

${generateQualityCriteria(report.quality.criteria)}

Coefficient de qualité = ${(report.quality.rate * 100.0).toFixed(2)}%

## Versions

${generateVersionsReport(report.tests.versions)}### Total rendus

Taux moyen = ${(sumOfRates / versionGradedCount).toFixed(2)}

Note intermédiaire = ${generateGrade(sumOfGrades)}

## Rendu complet

* Total: ${report.tests.full.total}
* Succès: ${report.tests.full.success}
* Taux: ${report.tests.full.rate.toFixed(1)}

Note intermédiaire = ${generateGrade(report.tests.full.grade)}

## Total 

Sous-total: ${generateGrade(report.subTotal)}

Impact qualité: ${(100.0 * (1 - report.quality.penalty)).toFixed(0)}%

Total: ${generateGrade(report.total, 2)}
`
}

function generateQualityCriteria(criteria: QualityCriterias) {
  return Object.entries(criteria)
    .map(([key, value]: [string, number]) => `* ${key}: ${value.toFixed(1)}`)
    .join("\n")
}

function generateVersionsReport(versions: VersionListReport) {
  return Object.entries(versions)
    .map(([key, value]: [string, VersionReport]) => generateVersionReport(key, value))
    .join("")
}

function generateVersionReport(version: string, report: VersionReport) {
  if (report.status === 'future')
    return ""
  
  return `### ${version}

* Total: ${report.total}
* Succès: ${report.success}
* Taux: ${report.rate.toFixed(1)}

`  
}

function generateGrade(grade: Grade, precision = 1) {
  return `${grade.value.toFixed(precision)}/${grade.maximum.toFixed(0)}`
}
