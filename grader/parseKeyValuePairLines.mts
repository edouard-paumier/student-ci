
export function parseKeyValuePairLines(lines: String[]) {
    return Object.fromEntries(lines
        .map(parseKeyValueLine)
        .map(([ key, value ]) => [ key, parseFloat(value) ])
        .filter(([ _, value ]) => typeof value === 'number' && !isNaN(value)))
  }
  
function parseKeyValueLine(line: String) {
const components = line.split(':', 2)
    .map(component => component.trim())

return components.length === 2
    ? components
    : [];
}
  