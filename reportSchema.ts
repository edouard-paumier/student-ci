export interface Report {
  quality: QualityReport;
  tests: TestReport;

  subTotal: Grade;
  total: Grade;
}

export interface QualityReport {
  criteria: QualityCriterias;
  rate: number;
  penalty: number;
}

export interface QualityCriterias {
  [criterias: string]: number;
}

export interface TestReport {
  versions: VersionListReport;
  full: FullTestReport;
}

export interface FullTestReport {
  total: number;
  success: number;
  rate: number;
  grade: Grade;
  error?: string;
}

export interface VersionListReport {
  [versionId: string]: VersionReport;
}

export type VersionReport = GradedVersionReport | FutureVersionReport

export interface GradedVersionReport {
  total: number;
  success: number;
  rate: number;
  grade: Grade;
  status: GradedVersionStatus;
  error?: string;
}

export interface FutureVersionReport {
  grade: FutureGrade;
  status: 'future'
}

export type GradedVersionStatus = 'locked' | 'current'

export interface Grade {
  value: number;
  maximum: number;
}

export interface FutureGrade {
  maximum: number;
}
