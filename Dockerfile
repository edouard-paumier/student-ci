FROM node:18-buster as build

WORKDIR /app

# Building displayer (and therefore template page for grader)
COPY [ "/package.json", "/app/package.json" ]
COPY [ "/package-lock.json", "/app/package-lock.json" ]

RUN npm ci
COPY [ "tsconfig.*", "/app/" ]
COPY [ "*.config.ts", "/app/" ]
COPY [ "*.ts", "/app/" ]
COPY [ "*.html", "/app/" ]
COPY [ "displayer/", "/app/displayer/" ]
COPY [ "grader/", "/app/grader/" ]

RUN npm run build

# Building grader

FROM node:18-buster as published

COPY --from=build [ "/app/package.json", "/app/" ]
COPY --from=build [ "/app/package-lock.json", "/app/" ]
WORKDIR /app
RUN npm ci --production
COPY --from=build [ "/app/build", "/app/" ]


FROM published as local-tests

ADD [ "/test-environment/", "/tmp/" ]
WORKDIR "/tmp/"
ENTRYPOINT [ "bash", "/tmp/entrypoint.sh" ]

FROM published
