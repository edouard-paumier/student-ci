import type { Report } from 'reportSchema'
import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import TotalGrade from '../TotalGrade.vue'

const report: Report = {
  quality: {
    rate: 0.61,
    criteria: {
      'Nommage variables': 0.9,
      'Usage adéquat des fonctionnalités du langage': 0.8,
      'Simplicité des fonctions': 0.7,
      'Simplicité des classes': 0.6,
      'Pas de dupplication de code': 0.5,
      'Séparation de responsabilités': 0.4,
      'Encapsulation': 0.3,
      'Couverture de tests': 0.7
    },
    penalty: .19,
  },
  tests: {
    versions: {
      'Version 1': {
        total: 5,
        success: 4,
        rate: 0.8,
        grade: {
          value: 1.14,
          maximum: 1.43
        },
        status: 'locked'
      },
      'Version 2': {
        total: 4,
        success: 2,
        rate: 0.5,
        grade: {
          value: 0.71,
          maximum: 1.43
        },
        status: 'locked'
      },
      'Version 3': {
        total: 10,
        success: 8,
        rate: 0.8,
        grade: {
          value: 1.14,
          maximum: 1.43
        },
        status: 'current'
      },
      'Version 4': {
        status: 'future',
        grade: {
          maximum: 1.43
        }
      },
      'Version 5': {
        status: 'future',
        grade: {
          maximum: 1.43
        }
      },
      'Version 6': {
        status: 'future',
        grade: {
          maximum: 1.43
        }
      },
      'Version 7': {
        status: 'future',
        grade: {
          maximum: 1.43
        }
      }
    },
    full: {
      total: 20,
      success: 18,
      rate: 0.9,
      grade: {
        value: 9,
        maximum: 10
      }
    }
  },
  subTotal: {
    value: 12,
    maximum: 14.29
  },
  total: {
    value: 9.67,
    maximum: 14.29
  }
}

describe('TotalGrade', () => {
  it('displays a proper header', () => {
    const wrapper = mount(TotalGrade, { 
      props: { report }
    })
    const header = wrapper.get('h2')
    expect(header.text()).toBe('Total')
  })
  it('displays its grade', () => {
    const wrapper = mount(TotalGrade, { 
      props: { report }
    })
    const gradeValue = wrapper.get('.total-grade .grade-value')
    expect(gradeValue.text()).toBe('9.67')
    
    const gradeMaximum = wrapper.get('.total-grade .grade-maximum')
    expect(gradeMaximum.text()).toBe('14.29')
  })

  it('displays its per-20 grade', () => {
    const wrapper = mount(TotalGrade, { 
      props: { report }
    })
    const gradeValue = wrapper.get('.equivalent-grade .grade-value')
    expect(gradeValue.text()).toBe('13.53')
    
    const gradeMaximum = wrapper.get('.equivalent-grade .grade-maximum')
    expect(gradeMaximum.text()).toBe('20')
  })
})
