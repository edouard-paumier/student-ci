import type { VersionReport } from 'reportSchema'
import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import VersionDisplay from '../VersionDisplay.vue'

const lockedVersion: VersionReport = {
  total: 4,
  success: 2,
  rate: 0.5,
  grade: {
    value: 0.71,
    maximum: 1.43
  },
  status: 'locked'
}

const currentVersion: VersionReport = {
  total: 10,
  success: 8,
  rate: 0.8,
  grade: {
    value: 1.14,
    maximum: 1.43
  },
  status: 'current'
}

const futureVersion: VersionReport = {
  status: 'future',
  grade: {
    maximum: 1.43
  }
}

describe('VersionDisplay', () => {
  for (const version of [lockedVersion, currentVersion]) {
    describe(`with ${version.status} report`, () => {
      it('displays a proper header', () => {
        const wrapper = mount(VersionDisplay, { 
          props: { versionName: 'Version 2', report: version }
        })
        const header = wrapper.get('h2')
        expect(header.text()).toBe('Version 2')
      })
      it('displays its rate', () => {
        const wrapper = mount(VersionDisplay, { 
          props: { versionName: 'Version 2', report: version }
        })
        const rate = wrapper.get('.rate')
        expect(rate.text()).toBe(version.rate === 0.8 ? '80%' : '50%')
      })
      it('displays its grade', () => {
        const wrapper = mount(VersionDisplay, { 
          props: { versionName: 'Version 2', report: version }
        })
        const gradeValue = wrapper.get('.grade-value')
        expect(gradeValue.text()).toBe(version.grade.value + '')
        
        const gradeMaximum = wrapper.get('.grade-maximum')
        expect(gradeMaximum.text()).toBe(version.grade.maximum + '')
      })
      it('displays its status', () => {
        const wrapper = mount(VersionDisplay, { 
          props: { versionName: 'Version 2', report: version }
        })
        const versionBlock = wrapper.get('article')
        expect(versionBlock.classes()).toContain(version.status)
      })
    })
  }
  describe(`with future report`, () => {
    it('displays a proper header', () => {
      const wrapper = mount(VersionDisplay, { 
        props: { versionName: 'Version 3', report: futureVersion }
      })
      const header = wrapper.get('h2')
      expect(header.text()).toBe('Version 3')
    })
    it('does not have any rate', () => {
      const wrapper = mount(VersionDisplay, { 
        props: { versionName: 'Version 3', report: futureVersion }
      })
      expect(wrapper.find('.rate').exists()).toBe(false)
    })
    it('displays its maximum grade', () => {
      const wrapper = mount(VersionDisplay, { 
        props: { versionName: 'Version 3', report: futureVersion }
      })
      
      const gradeMaximum = wrapper.get('.grade-maximum')
      expect(gradeMaximum.text()).toBe(futureVersion.grade.maximum + '')
    })
    it('does not have grade value', () => {
      const wrapper = mount(VersionDisplay, { 
        props: { versionName: 'Version 3', report: futureVersion }
      })
      const gradeValue = wrapper.get('.grade-value')
      expect(gradeValue.text()).toBe("—")
    })
    it('displays its status', () => {
      const wrapper = mount(VersionDisplay, { 
        props: { versionName: 'Version 3', report: futureVersion }
      })
      const versionBlock = wrapper.get('article')
      expect(versionBlock.classes()).toContain('future')
    })
  })
})
