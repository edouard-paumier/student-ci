import type { QualityReport } from 'reportSchema'
import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import QualityDisplay from '../QualityDisplay.vue'

const report: QualityReport = {
  rate: .61,
  criteria: {
    'Nommage variables': 0.9,
    'Usage adéquat des fonctionnalités du langage': 0.8,
    'Simplicité des fonctions': 0.7,
    'Simplicité des classes': 0.6,
    'Pas de dupplication de code': 0.5,
    'Séparation de responsabilités': 0.4,
    'Encapsulation': 0.3,
    'Couverture de tests': 0.7
  },
  penalty: .19
}

describe('QualityDisplay', () => {
  it('displays a proper header', () => {
    const wrapper = mount(QualityDisplay, { 
      props: { report }
    })
    const header = wrapper.get('h2')
    expect(header.text()).toBe('Qualité')
  })
  it('displays its rate', () => {
    const wrapper = mount(QualityDisplay, { 
      props: { report }
    })
    const rate = wrapper.get('[data-test="rate"]')
    expect(rate.text()).toBe('61%')
  })
  it('displays its penalty', () => {
    const wrapper = mount(QualityDisplay, { 
      props: { report }
    })
    const rate = wrapper.get('.penalty')
    expect(rate.text()).toContain('19%')
  })
  it('displays its criteria', () => {
    const wrapper = mount(QualityDisplay, { 
      props: { report }
    })
    const criterion = wrapper.get('[data-test="methodSimplicity"]')
    const criterionHeader = criterion.get('.criterion')
    expect(criterionHeader.text()).toBe('Simplicité des méthodes:')

    const criterionRate = criterion.get('.cirterion-rate')
    expect(criterionRate.text()).toBe('70%')
  })
})
