import type { Report } from '../reportSchema'

import { createSSRApp } from 'vue'
import { renderToString } from 'vue/server-renderer'
import App from './App.vue'

type GradeReport = {
  branch: string;
  date: string;
  report: Report
}

export async function render(gradeReport: GradeReport) {
  const app = createSSRApp(App, gradeReport)
  return await renderToString(app)
}
